import pika
from pika.adapters.blocking_connection import BlockingChannel

import vars

NAME = 'jc_pc_2'
binding_keys = ['project.2.#']
app_id = 'jc_pc_2'
# parameters = pika.URLParameters('amqp://guest:G0rDanso@185.72.22.84:5672/%2F')
parameters = pika.URLParameters('amqp://guest:guest@localhost:5673/%2F')

def on_message(ch: BlockingChannel, method, properties, body):
    if properties.app_id == app_id:
        print(f" [x] {NAME} Skipped")
    else:
        print(f" [x] {NAME} Received {body}")
    ch.basic_ack(delivery_tag=method.delivery_tag)


def process(*args, **kwargs):
    connection = pika.BlockingConnection(parameters)

    binding = kwargs.get('binding_keys', binding_keys)
    channel = connection.channel()

    channel.exchange_declare(exchange=vars.exchange_name,
                             exchange_type=vars.exchange_type,
                             durable=True)

    # Change to named queue with durable True
    result = channel.queue_declare('pc_2_inbox', durable=True)
    queue_name = result.method.queue

    for binding_key in binding:
        channel.queue_bind(
            exchange=vars.exchange_name,
            queue=queue_name,
            routing_key=binding_key
        )

    print(f' [*] {NAME} {binding} Waiting for messages. To exit press CTRL+C')

    channel.basic_consume(
        queue=queue_name,
        on_message_callback=on_message,
        auto_ack=False
    )

    channel.start_consuming()
