@listen('create_item')
def on_remote_create_item(data):
    """
    Callback when item create on remote host
    :param data:
    :return:
    """
    create_item_in_db(data)  # Create item in local db with id recieved from remote server


def create_item(data):
    item = create_item_in_db(data)  # Create item in local db
    sync_create_item(item)  # Send sync request to remove server
