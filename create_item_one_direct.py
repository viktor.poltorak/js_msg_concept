@listen('create_item')
def on_remote_create_item(data):
    """
    Callback when item create on remote host
    :param data:
    :return:
    """
    create_item_in_db(data)  # Create item in local db with id recieved from remote server


def create_item(data):
    if is_main():  # Check if this item should be create here
        item = create_item_in_db(data)  # Create item in local db
        sync_create_item(item)  # Send sync request to remove server
    else:
        remote_create_item(data)  # Send request to create remove item
