#!/usr/bin/env python
import datetime
import json
import pika
import vars
import sys

# parameters = pika.URLParameters('amqp://guest:G0rDanso@185.72.22.84:5672/%2F')
parameters = pika.URLParameters('amqp://guest:guest@localhost:5673/%2F')

connection = pika.BlockingConnection(parameters)
channel = connection.channel()

channel.exchange_declare(exchange=vars.exchange_name,
                         exchange_type=vars.exchange_type,
                         durable=True)

routing_key = 'project.1'
# routing_key = sys.argv[1] if len(sys.argv) > 1 else 'common.info'

message = json.dumps(
    {
        "time": "2020-05-13 17:33:59.326533",
        "event": "measurement",
        "data": {
            "id": "a062a424-6831-42aa-accd-4989372639c6",
            "time": "2020-04-24 07:02:41",
            "main_force": 1174.928100906771,
            "main_pressure": 40.283203136,
            "tunnel_length": 274992,
        }
    }
)

channel.basic_publish(
    exchange=vars.exchange_name,
    routing_key=routing_key,
    body=message,
    properties=pika.BasicProperties(
        content_type='application/json',
        app_id='m_service',
        delivery_mode=2,  # make message persistent
    ),
)
print(" [x] Sent %r:%r" % (routing_key, message))
connection.close()
