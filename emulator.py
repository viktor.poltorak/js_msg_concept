from threading import Thread

import j_cloud
import pc_1
import pc_2
import m_service

if __name__ == "__main__":
    print('Starting emulation')
    print('Starting cloud consumer')
    thread_pc_1 = Thread(target=pc_1.process)  # JC PC for project 1
    thread_pc_2 = Thread(target=pc_2.process)  # JC PC for project 2
    thread_cloud = Thread(target=j_cloud.process)  # JC Cloud
    thread_service = Thread(target=m_service.process)  # Measurements service
    thread_pc_1.start()
    thread_pc_2.start()
    thread_cloud.start()
    thread_service.start()
