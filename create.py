#!/usr/bin/env python
import json
import sys
import requests
from yurl import URL


class RabbitManager:
    def __init__(self, broker_url, broker_internal_host=None):
        """

        :param broker_url: Broker manager url
        :param broker_internal_host: Needed if your broker run with docker and needed to set up federation in docker network
        """
        self.vhost = '%2f'
        self.broker_url = broker_url
        self.broker_internal_host = broker_internal_host

    def get_url(self, path):
        return f"{self.broker_url}{path}"

    def get(self, url):
        result = requests.get(self.get_url(url))
        if result.status_code == 200:
            try:
                return json.loads(result.content)
            except json.decoder.JSONDecodeError:
                print('Error json loads', result.content)
                return result.content
        else:
            return None

    def put(self, url, data):
        result = requests.put(self.get_url(url), json.dumps(data))
        if result.status_code in [201, 204]:
            try:
                return json.loads(result.content)
            except json.decoder.JSONDecodeError:
                print('Error json loads', result.content)
                return result.content
        else:
            print(result.status_code)
            print(result.content)
            return None

    def get_upstreams(self):
        return self.get(f'/api/parameters/federation-upstream/{self.vhost}/')

    def get_policies(self):
        return self.get(f'/api/policies/{self.vhost}/')

    def create_upstream(self, name, remote_uri):
        url = f'/api/parameters/federation-upstream/{self.vhost}/{str(name)}/'

        upstreams = self.get_upstreams()
        upstream = next((x for x in upstreams if x['value']['uri'] == remote_uri), None)

        if upstream:
            # Upstream already exists
            print('Upstream exists')
            return upstream

        data = {
            'value': {
                'ack-mode': 'on-confirm',
                'trust-user-id': False,
                'uri': remote_uri
            },
            'vhost': self.vhost,
            'component': 'federation-upstream',
            'name': name
        }
        return self.put(url, data)

    def create_policy(self, name, pattern):
        url = f'/api/policies/{self.vhost}/{name}'

        policies = self.get_policies()
        policy = next((x for x in policies if x['name'] == name), None)

        if policy:
            print('Policy exists')
            return policy

        data = {
            "pattern": pattern,
            "definition": {"federation-upstream-set": "all"},
            "priority": 0,
            "apply-to": "exchanges"
        }
        return self.put(url, data=data)

    def get_amqp_url(self):
        url = URL(self.broker_url)

        if self.broker_internal_host:
            url = url.replace(host=self.broker_internal_host)

        return str(url.replace(scheme='amqp', port=int(url.port) - 10000))


def setup_federation(broker_1_url, broker_2_url, name, pattern, **kwargs):
    broker1 = RabbitManager(broker_1_url, 'rabbitmq')
    broker2 = RabbitManager(broker_2_url, 'rabbitmq_second')

    broker1.create_upstream(name, broker2.get_amqp_url())
    broker1.create_policy(name=name, pattern=pattern)

    broker2.create_upstream(name, broker1.get_amqp_url())
    broker2.create_policy(name=name, pattern=pattern)
    print(f'Set Federation between {broker_1_url} and {broker_2_url} finished')


if __name__ == "__main__":
    action = sys.argv[1] if len(sys.argv) > 1 else None
    param_1 = sys.argv[2] if len(sys.argv) > 2 else None
    param_2 = sys.argv[3] if len(sys.argv) > 3 else None
    param_3 = sys.argv[4] if len(sys.argv) > 4 else None
    param_4 = sys.argv[5] if len(sys.argv) > 5 else None

    if action == 'setup_federation':
        print('  [*] Set up federation')
        setup_federation(param_1, param_2, name=param_3, pattern=param_4)
    else:
        print("""
        
        Usage: 
             ./create.py setup_federation {broker_1_url} {broker_2_url} {name} {pattern}
        
        setup_federation - creates federation link between brokers with type exchange 
        No a lot of setting for now.        
        policy and upstream with {name} 
        set policy pattern to {pattern}
        
        Check if upstream exists by uri
        Check if policy exists with by {name}  
        """)
